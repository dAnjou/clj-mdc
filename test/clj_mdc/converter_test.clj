(ns clj-mdc.converter-test
  (:require [clojure.test :refer :all]
            [clj-mdc.core :refer :all]
            [clojure.string :refer [includes? ends-with?]]
            [clojure.tools.logging :as log])
  (:import (clj_mdc TestAppender)))

(deftest converter-test
  (testing "full MDC"
    (with-mdc {"number"  42
               "string"  "foo"
               "boolean" true
               "nil"     nil
               "list"    [1 2]
               "map"     {:foo 42}}
      (log/log "fullmdc" :info nil "something"))
    (with-open [appender (TestAppender.)]
      (is (= "%mdc ~ %msg" (.getPattern appender)))
      (let [actual (first (TestAppender/getEvents))]
        (is (ends-with? actual " ~ something"))
        (is (includes? actual "number=42"))
        (is (includes? actual "string=foo"))
        (is (includes? actual "boolean=true"))
        (is (includes? actual "nil=null"))
        (is (includes? actual "list=[1 2]"))
        (is (includes? actual "map={:foo 42}")))))

  (testing "single entry"
    (with-mdc {"test" 42}
      (log/log "singleentry" :info nil "something"))
    (with-open [appender (TestAppender.)]
      (is (= "%mdc{test} ~ %msg" (.getPattern appender)))
      (let [actual (first (TestAppender/getEvents))]
        (is (= "42 ~ something" actual))))))
