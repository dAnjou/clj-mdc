(ns clj-mdc.core-test
  (:require [clojure.test :refer :all]
            [clj-mdc.core :refer :all])
  (:import (org.slf4j MDC)))

(deftest with-mdc-test
  (testing "should put key/value into MDC and remove it afterwards"
    (MDC/clear)
    (with-mdc {"foo"  42
               :lorem "ipsum"}
      (is (= {"foo"   42
              "lorem" "ipsum"} (MDC/getCopyOfContextMap))))
    (is (= {} (MDC/getCopyOfContextMap))))

  (testing "should handle multiple forms"
    (MDC/clear)
    (with-mdc {"lorem" "ipsum"}
      (identity "just another form")
      (is (= {"lorem" "ipsum"} (MDC/getCopyOfContextMap)))))

  (testing "should handle values passed via symbol"
    (MDC/clear)
    (let [foo {"key" "value"}]
      (with-mdc foo
        (is (= {"key" "value"} (MDC/getCopyOfContextMap))))))

  (testing "should handle nesting"
    (MDC/clear)
    (with-mdc {"lorem" "ipsum"}
      (is (= {"lorem" "ipsum"} (MDC/getCopyOfContextMap)))
      (with-mdc {"dolor" "sit"}
        (is (= {"lorem" "ipsum"
                "dolor" "sit"} (MDC/getCopyOfContextMap))))))

  (testing "should keep previous key/value in MDC"
    (MDC/clear)
    (MDC/put "lorem" "ipsum")
    (with-mdc {"key" "value"}
      (is (= {"key"   "value"
              "lorem" "ipsum"}
             (MDC/getCopyOfContextMap))))
    (is (= {"lorem" "ipsum"} (MDC/getCopyOfContextMap))))

  (testing "should keep previous key/value in MDC even if we pass empty map"
    (MDC/clear)
    (MDC/put "lorem" "ipsum")
    (with-mdc {}
      (is (= {"lorem" "ipsum"}
             (MDC/getCopyOfContextMap))))
    (is (= {"lorem" "ipsum"} (MDC/getCopyOfContextMap))))

  (testing "should overwrite previous key/value in MDC"
    (MDC/clear)
    (MDC/put "lorem" "ipsum")
    (with-mdc {"lorem" "banana"}
      (is (= {"lorem" "banana"}
             (MDC/getCopyOfContextMap))))
    (is (= {"lorem" "ipsum"} (MDC/getCopyOfContextMap))))

  (testing "should handle empty map"
    (MDC/clear)
    (is (= nil (MDC/getCopyOfContextMap)))
    (with-mdc {} (is (= nil (MDC/getCopyOfContextMap))))
    (is (= nil (MDC/getCopyOfContextMap))))

  (testing "should handle nil"
    (MDC/clear)
    (is (= nil (MDC/getCopyOfContextMap)))
    (with-mdc nil (is (= nil (MDC/getCopyOfContextMap))))
    (is (= nil (MDC/getCopyOfContextMap))))

  (testing "should return result of last form in body"
    (is (= 42 (with-mdc {}
                "lorem"
                "ipsum"
                42))))

  (testing "should not catch exception thrown in body"
    (is (thrown? Exception (with-mdc {}
                             (throw (ex-info "boom" {})))))))
