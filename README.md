# clj-mdc

Utils for dealing with MDC

[![Clojars](https://img.shields.io/clojars/v/de.danjou/clj-mdc.svg)](https://clojars.org/de.danjou/clj-mdc)
[![cljdoc](https://cljdoc.org/badge/de.danjou/clj-mdc)](https://cljdoc.org/d/de.danjou/clj-mdc/CURRENT)


## Features

- `with-mdc` - macro for adding values to the MDC
- `MDCConverter` - custom converter that doesn't crash when passed non-string values (see #1)