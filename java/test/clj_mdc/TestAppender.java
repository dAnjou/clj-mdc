package clj_mdc;

import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.OutputStreamAppender;

import java.io.Closeable;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class TestAppender extends OutputStreamAppender<ILoggingEvent> implements Closeable {
    private static List<String> events = new ArrayList<>();
    private static String pattern;

    static class NullOutputStream extends OutputStream {
        @Override
        public void write(int b) {
        }
    }

    @Override
    public void start() {
        setOutputStream(new NullOutputStream());
        super.start();
    }

    @Override
    protected void subAppend(ILoggingEvent event) {
        if (!isStarted()) {
            return;
        }
        pattern = ((PatternLayoutEncoder) this.encoder).getPattern();
        byte[] byteArray = this.encoder.encode(event);
        events.add(new String(byteArray, StandardCharsets.UTF_8));
    }

    public static List<String> getEvents() {
        return events;
    }

    public static void clearEvents() {
        events.clear();
    }

    @Override
    public void close() {
        clearEvents();
    }

    public String getPattern() {
        return pattern;
    }
}
