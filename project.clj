(defproject de.danjou/clj-mdc "0.2.11"
  :description "Utils for dealing with MDC"
  :url "https://gitlab.com/dAnjou/clj-mdc"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.slf4j/slf4j-api "2.0.16"]
                 [ch.qos.logback/logback-classic "1.5.16"]
                 [org.clojure/tools.logging "1.3.0" :exclusions [org.clojure/clojure]]]
  :java-source-paths ["java/src" "java/test"]
  :jar-exclusions [#"java/test"]
  :repl-options {:init-ns clj-mdc.core}
  :profiles {:common {:dependencies [[org.clojure/clojure "1.12.0"]]}
             :dev    [:common]
             :test   [:common]}
  :plugins [[lein-project-version "0.1.0"]
            [lein-ancient "0.7.0"]
            [lein-cljfmt "0.9.2"]]
  :scm {:name "git" :url "https://gitlab.com/dAnjou/clj-mdc"}
  :deploy-repositories [["clojars" {:url           "https://clojars.org/repo"
                                    :username      :env/clojars_username
                                    :password      :env/clojars_password
                                    :sign-releases false}]])
