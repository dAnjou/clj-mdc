(ns clj-mdc.core
  (:import (org.slf4j MDC)))

(defmacro with-mdc [ctx & body]
  `(if (empty? ~ctx)
     (do ~@body)
     (let [ctx# (reduce-kv (fn [m# k# v#] (assoc m# (name k#) v#)) {} ~ctx)
           previous-context# (into {} (MDC/getCopyOfContextMap))]
       (try
         (MDC/setContextMap (merge previous-context# ctx#))
         ~@body
         (finally (MDC/setContextMap previous-context#))))))
